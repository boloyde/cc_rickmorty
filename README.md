Click on a table row to see more information and <br>
an image of the selected Rick & Morty Character

Frontend development using React for 'Cultura Colectiva'<br>

Developed by 'Jose Avalos'

## Available Scripts

### `yarn install`

In the project directory, you can run:

### `npm start`
### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.