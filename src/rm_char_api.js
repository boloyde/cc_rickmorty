function fetchJson(url, options) {
    return fetch(
        url,
        Object.assign({
            credentials: "same-origin"
        }, options))
        .then(response => {
            return response.json();
        });
}
export function getCharacters(page = '1') {
    return fetchJson('https://rickandmortyapi.com/api/character/?page='+page);
}
export function getCharacter(id) {
    return fetchJson('https://rickandmortyapi.com/api/character/'+id);
}