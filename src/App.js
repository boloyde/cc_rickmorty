import React, {Component} from 'react';
import './App.css';
import {getCharacters} from "./rm_char_api";
import {getCharacter} from "./rm_char_api";
import RMCharacters from "./RMCharacters";

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            highlightedRowId: null,
            characters: [],
            charsInfo: [],
            charDetail: null,
        };
        this.handleRowClick = this.handleRowClick.bind(this);
        this.handlePageTurn = this.handlePageTurn.bind(this);
    }
    componentDidMount(){
        getCharacters()
            .then((data) => {
                const pageInfo = {prev: data.info.prev.substr(-1), next: data.info.next.substr(-1)};
                this.setState({
                    characters: data.results,
                    charsInfo: pageInfo,
                });
            })
    }
    handleRowClick(charId){
        this.setState({highlightedRowId: charId});
        getCharacter(charId)
            .then((data) => {
                this.setState({
                    charDetail: data
                })
            })
    }
    handlePageTurn(page){
        getCharacters(page)
            .then((data) => {
                const pageInfo = {prev: data.info.prev.substr(-1), next: data.info.next.substr(-1)};
                this.setState({
                    characters: data.results,
                    charsInfo: pageInfo,
                });
            })
    }

    render() {
        const {highlightedRowId, characters, charsInfo, charDetail} = this.state;

        return <RMCharacters
            {...this.state}
            onRowClick={this.handleRowClick}
            onPageTurn={this.handlePageTurn}
        />
    }
}
