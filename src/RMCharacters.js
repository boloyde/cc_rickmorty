import React from 'react';

export default function rmCharacters(props) {
    const {
        highlightedRowId,
        characters,
        charsInfo,
        charDetail,
        onRowClick,
        onPageTurn,
    } = props;

    const handlePageClick = function (event, page) {
        event.preventDefault();

        onPageTurn(page);
    };

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-6">
                    <h2>
                        Rick & Morty Characters
                    </h2>
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <td>Name</td>
                            <td>Gender</td>
                            <td>Species</td>
                            <td>Status</td>
                        </tr>
                        </thead>
                        <tbody>
                        {characters.map((character)=>{
                            return (
                                <tr
                                key={character.id}
                                className={highlightedRowId === character.id ? 'table-info':''}
                                onClick={()=>onRowClick(character.id)}
                                >
                                    <td>{character.name}</td>
                                    <td>{character.gender}</td>
                                    <td>{character.species}</td>
                                    <td>{character.status}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                </div>
                {charDetail &&
                <div className="col-md-4 offset-md-1">
                    <div className="card">
                        <img src={charDetail.image} />
                        <div className="detail-container text-center">
                            <p className="display-3">{charDetail.name}</p>
                            <p className="text-left"><b>Origin: </b>{charDetail.origin.name}</p>
                            <p className="text-left"><b>Location: </b>{charDetail.location.name}</p>
                            <p className="text-left"><b>Species/Gender: </b>{charDetail.species} {charDetail.gender}</p>
                            <p className="display-4 text-danger">{charDetail.status}</p>
                        </div>
                    </div>
                </div>
                }
            </div>
            <div className="row">
                {charsInfo.prev &&
                <a href="#"
                   className="d-inline btn btn-secondary"
                    onClick={(event) => handlePageClick(event, charsInfo.prev)}
                >Previous Page</a>}
                {charsInfo.next &&
                <a href="#"
                   className="d-inline btn btn-secondary"
                    onClick={(event)=> handlePageClick(event, charsInfo.next)}
                >Next Page</a>}
            </div>
        </div>
    )
}
